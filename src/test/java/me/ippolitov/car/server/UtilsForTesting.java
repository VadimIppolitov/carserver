package me.ippolitov.car.server;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class UtilsForTesting {
    static InetSocketAddress loopbackWithEphemeralPort() {
        return new InetSocketAddress(InetAddress.getLoopbackAddress(), 0);
    }

}
