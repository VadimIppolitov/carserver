package me.ippolitov.car.server;

import com.diffplug.common.base.Errors;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.testng.Assert.*;

public class CarServerTest {
    private CarServer server;
    private List<DummyCarClient> clients = new ArrayList<>(3);
    private List<DummyCarClient> staticPeers = new ArrayList<>(3);

    @BeforeMethod
    public void setup() throws IOException {
        setupServer(genTestSettings());
    }

    @AfterMethod
    public void teardown() throws IOException {
        teardownServer();
        clients.forEach(Errors.rethrow().wrap(DummyCarClient::close));
        clients.clear();
        staticPeers.forEach(Errors.rethrow().wrap(DummyCarClient::close));
        staticPeers.clear();
    }

    @Test
    public void testInitAndNoBogusPackets() throws IOException {
        assertNotNull(server.boundTo());
        server.runOnce();
        assertEquals(server.peers.size(), 0);
    }

    @Test
    public void testSingleClientNoLoopback() throws IOException {
        createClients(1);
        ByteBuffer packet = clients.get(0).sendToServer();
        server.runOnce();
        clients.get(0).expectNoPackets();
        assertEquals(server.peers.size(), 1);
    }

    @Test
    public void testPacketExchange() throws IOException {
        createClients(2);
        ByteBuffer packet1 = clients.get(0).sendToServer();
        ByteBuffer packet2 = clients.get(1).sendToServer();
        server.runOnce();
        server.runOnce();
        assertEquals(server.peers.size(), 2);
        clients.get(0).expectPackets(packet2);
        clients.get(1).expectNoPackets();  //  packet1 was not sent to any clients
        ByteBuffer packet3 = clients.get(0).sendToServer();
        server.runOnce();
        clients.get(1).expectPackets(packet3);  //  now connection is two-way
        clients.get(0).expectNoPackets();       //  still no loopback
    }

    @Test
    public void testMaxPeers() throws IOException {
        resetServer(s -> s.setMaxPeers(2));

        testPacketExchange();  //  Do the test again, but now with maxPeers = 2. Should work!

        //  Now try to exceed the maxPeers limit and verify that the packet is discarded
        createClients(1);
        ByteBuffer packet = clients.get(2).sendToServer();
        server.runOnce();
        clients.get(0).expectNoPackets();
        clients.get(1).expectNoPackets();
        clients.get(2).expectNoPackets();
        assertEquals(server.peers.size(), 2);
    }

    @Test
    public void testSinglePeerTimeoutAndRecovery() throws IOException, InterruptedException {
        resetServer(s -> s.setPeerTimeout(300));
        createClients(1);
        clients.get(0).sendToServer();
        server.runOnce();
        assertEquals(server.peers.size(), 1);
        Thread.sleep(server.getSettings().getPeerTimeout());
        server.runOnce();
        assertEquals(server.peers.size(), 0);
        clients.get(0).sendToServer();
        server.runOnce();
        assertEquals(server.peers.size(), 1);
    }

    @Test
    public void testTwoPeersOneTimeoutAndRecovery() throws IOException, InterruptedException {
        resetServer(s -> s.setPeerTimeout(800));
        createClients(2);
        ByteBuffer packet1 = clients.get(0).sendToServer(); // this one is lost
        ByteBuffer packet2 = clients.get(1).sendToServer();
        server.runOnce();
        server.runOnce();
        assertEquals(server.peers.size(), 2);
        Thread.sleep(server.getSettings().getPeerTimeout() / 2);
        server.runOnce();
        assertEquals(server.peers.size(), 2);
        ByteBuffer packet3 = clients.get(0).sendToServer();  //  to prevent client(0) from timing out
        server.runOnce();
        clients.get(1).expectPackets(packet3);
        clients.get(0).expectPackets(packet2);
        Thread.sleep(server.getSettings().getPeerTimeout() / 2);
        server.runOnce();
        assertEquals(server.peers.size(), 1);
        ByteBuffer packet4 = clients.get(1).sendToServer();
        server.runOnce();
        clients.get(0).expectPackets(packet4);
        clients.get(1).expectNoPackets();
        assertEquals(server.peers.size(), 2);
    }

    @Test
    public void testBadMagic() throws IOException {
        createClients(2);
        ByteBuffer packet1 = clients.get(0).sendToServer(); // this one is lost
        ByteBuffer packet2 = clients.get(1).sendToServerWithBadMagic(); // this one should be dropped
        server.runOnce();
        server.runOnce();
        assertEquals(server.peers.size(), 1);
        clients.get(0).expectNoPackets();
        ByteBuffer packet3 = clients.get(0).sendToServer(); // this one should also be lost
        server.runOnce();
        assertEquals(server.peers.size(), 1);
        clients.get(1).expectNoPackets();
    }

    @Test
    public void testStaticPeer() throws IOException {
        createClients(1);
        DummyCarClient staticPeer = createStaticPeer();
        ByteBuffer packet1 = clients.get(0).sendToServer(); // this one is delivered to the static peer 
        server.runOnce();
        assertEquals(server.peers.size(), 1);
        clients.get(0).expectNoPackets();
        staticPeer.expectPackets(packet1);
        ByteBuffer packet2 = staticPeer.sendToServer();
        server.runOnce();
        assertEquals(server.peers.size(), 2); // For now the static peer also registers as a regular peer
        clients.get(0).expectPackets(packet2);
        staticPeer.expectNoPackets();
        ByteBuffer packet3 = clients.get(0).sendToServer(); // this one is delivered to the static peer two times (FIXME later) 
        server.runOnce();
        staticPeer.expectPackets(packet3, packet3);
        clients.get(0).expectNoPackets();
    }

    @Test
    public void testDuplicatePacket() throws IOException {
        createClients(2);
        ByteBuffer packet1 = clients.get(0).sendToServer();
        ByteBuffer packet2 = clients.get(1).sendToServer();
        server.runOnce();
        server.runOnce();
        assertEquals(server.peers.size(), 2);
        clients.get(0).expectPackets(packet2);
        clients.get(1).expectNoPackets();

        clients.get(0).sendToServer(packet1);  //  Again, should be dropped
        server.runOnce();
        clients.get(1).expectNoPackets();
        clients.get(0).expectNoPackets();
    }

    private void resetServer(Consumer<Settings> settingsCustomizer) throws IOException {
        teardownServer();
        Settings settings = genTestSettings();
        settingsCustomizer.accept(settings);
        setupServer(settings);
    }

    private void createClients(int howMany) throws IOException {
        for (int i = 0; i < howMany; ++i) {
            clients.add(new DummyCarClient(server));
        }
    }

    private DummyCarClient createStaticPeer() throws IOException {
        DummyCarClient staticPeer = new DummyCarClient(server);
        server.getSettings().addStaticPeer(HostPort.fromSocketAddress(staticPeer.boundTo()));
        staticPeers.add(staticPeer);
        return staticPeer;
    }

    private Settings genTestSettings() {
        Settings settings = new Settings();
        settings.setBindAddress(UtilsForTesting.loopbackWithEphemeralPort());  //  Allow the OS to bind to a free port
        settings.setMaxPeers(3);
        settings.setSelectTimeout(100);
        return settings;
    }

    private void setupServer(Settings settings) throws IOException {
        server = new CarServer(settings);
    }

    private void teardownServer() throws IOException {
        server.close();
        server = null;
    }
}