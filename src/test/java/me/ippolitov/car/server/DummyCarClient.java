package me.ippolitov.car.server;

import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static me.ippolitov.car.server.Utils.*;
import static org.testng.Assert.*;

@Slf4j
class DummyCarClient implements Closeable {

    private final DatagramChannel channel;
    private final Selector selector;
    private InetSocketAddress bindAddress;
    List<Buffer> sentPackets = new ArrayList<>();
    private CarServer server;

    DummyCarClient(CarServer server) throws IOException {
        this.server = server;
        channel = DatagramChannel.open(StandardProtocolFamily.INET);
        channel.bind(UtilsForTesting.loopbackWithEphemeralPort());
        this.bindAddress = (InetSocketAddress) channel.getLocalAddress();
        selector = Selector.open();
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ);
        log.info("Created client {}", boundTo());
    }

    InetSocketAddress boundTo() {
        return bindAddress;
    }

    public ByteBuffer sendToServer() throws IOException {
        ByteBuffer buffer = randomPacket();
        return sendToServer(buffer);
    }

    public ByteBuffer sendToServerWithBadMagic() throws IOException {
        ByteBuffer buffer = randomBadPacket();
        return sendToServer(buffer);
    }

    public ByteBuffer sendToServer(ByteBuffer buffer) throws IOException {
        InetSocketAddress target = new InetSocketAddress(InetAddress.getLoopbackAddress(), server.boundTo().getPort());
        channel.send(buffer, target);
        buffer.flip();
        sentPackets.add(buffer);
        log.info("Sent from {}: {}", boundTo(), debugBufStr(buffer));
        return buffer;
    }

    private ByteBuffer randomPacket() {
        return randomPacket(Settings.MAGIC);
    }

    private ByteBuffer randomBadPacket() {
        String magic = "BAD";
        return randomPacket(magic);
    }

    private ByteBuffer randomPacket(String magic) {
        String random = UUID.randomUUID().toString();
        return ByteBuffer.wrap((magic + random).getBytes(StandardCharsets.UTF_8));
    }

    public void expectNoPackets() throws IOException {
        expectPackets();
    }

    public void expectPackets(ByteBuffer firstPacket, ByteBuffer... morePackets) throws IOException {
        List<ByteBuffer> packets = new LinkedList<>();
        packets.add(firstPacket);
        packets.addAll(Arrays.asList(morePackets));
        expectPackets(packets.toArray(new ByteBuffer[packets.size()]));
    }

    private void expectPackets(ByteBuffer... packets) throws IOException {
        for (int i = 0; i <= packets.length; i++) {
            selector.select(server.getSettings().getSelectTimeout());
            final Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            if (i < packets.length && !iterator.hasNext()) {
                fail(String.format("%s missing packet #%d of %d", boundTo(), i+1, packets.length));
            }
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                if (key.isValid() && key.isReadable()) {
                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    assert key.channel() == channel;
                    assertNotNull(channel.receive(buffer));
                    buffer.flip();
                    if (i == packets.length) {
                        fail(String.format("%s got unexpected packet #%d: %s", boundTo(), i+1, debugBufStr(buffer)));
                    }
                    assertEqualBuffers(buffer, packets[i], "Got packet with unexpected contents");
                    log.info("{} got expected packet #{}: {}", boundTo(), i+1, debugBufStr(buffer));
                }
                iterator.remove();
            }
        }
    }

    private void assertEqualBuffers(ByteBuffer actual, ByteBuffer expected, String message) {
        if (!actual.equals(expected)) {
            fail(message + ", expected: " + debugBufStr(expected) + " but was: " + debugBufStr(actual));
        }
    }

    @Override
    public void close() throws IOException {
        channel.close();
        selector.close();
    }
}
