package me.ippolitov.car.server;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PacketHistory {
    private final MessageDigest digest;
    private final int capacity;
    //  This set contains hashes, not real packets!
    private final Set<ByteArrayWrapper> seenHashes;

    public PacketHistory(int capacity) {
        this.capacity = capacity;
        try {
            digest = MessageDigest.getInstance("sha-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        seenHashes = new HashSet<>(capacity);
    }

    public boolean addToHistory(ByteBuffer byteBuffer) {
        if (seenHashes.size() >= capacity) {
            //  Removes effectively random item due to hashing
            Iterator<ByteArrayWrapper> iterator = seenHashes.iterator();
            iterator.next();
            iterator.remove();
        }
        ByteArrayWrapper hash = calcHash(byteBuffer);
        return seenHashes.add(hash);
    }

    private ByteArrayWrapper calcHash(ByteBuffer value) {
        digest.reset();
        digest.update(value.asReadOnlyBuffer());
        return new ByteArrayWrapper(digest.digest());
    }

    private static class ByteArrayWrapper {
        private byte[] bytes;

        private ByteArrayWrapper(byte[] bytes) {
            this.bytes = bytes;
        }

        @Override
        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        public boolean equals(Object o) {
            return Arrays.equals(bytes, ((ByteArrayWrapper) o).bytes);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(bytes);
        }
    }
}
