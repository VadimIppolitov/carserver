package me.ippolitov.car.server;

import org.apache.commons.cli.*;

import java.io.IOException;

public class Main {

    private static final String PEER_OPTION = "peer";
    private static final String HELP_OPTION = "help";

    public static void main(String[] args) throws IOException, ParseException {
        CommandLine cmdLine = parseCommandLine(args);

        Settings settings = new Settings();
        if (cmdLine.hasOption(PEER_OPTION)) {
            for (String peer : cmdLine.getOptionValues(PEER_OPTION)) {
                settings.addStaticPeer(HostPort.parse(peer));
            }
        }
        CarServer carServer = new CarServer(settings);
        carServer.run();
    }

    private static CommandLine parseCommandLine(String[] args) throws ParseException {
        Options options = new Options();
        Option peersOption = Option.builder()
                .desc("Static peer, may specify several")
                .longOpt(PEER_OPTION)
                .hasArgs()
                .argName("host:port")
                .build();
        Option helpOption = Option.builder()
                .desc("Print help")
                .longOpt(HELP_OPTION)
                .build();

        options.addOption(peersOption);
        options.addOption(helpOption);

        CommandLine cmdLine = new DefaultParser().parse(options, args);
        if (cmdLine.hasOption(HELP_OPTION)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("car-server", options, true);
            System.exit(1);
        }
        return cmdLine;
    }
}
