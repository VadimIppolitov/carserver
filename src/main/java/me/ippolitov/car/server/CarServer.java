package me.ippolitov.car.server;

import com.diffplug.common.base.DurianPlugins;
import com.diffplug.common.base.Errors;
import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static me.ippolitov.car.server.Utils.*;

@Slf4j
public class CarServer implements PacketHandler, Closeable {

    static {
        DurianPlugins.register(Errors.Plugins.Log.class, error -> log.error("Unexpected error", error));
    }

    private final NetworkService networkService;
    private final PacketHistory packetHistory = new PacketHistory(1_000_000);
    final Map<SocketAddress, Peer> peers = new HashMap<>();
    private final Settings settings;

    public CarServer(Settings settings) throws IOException {
        this.settings = settings;
        this.networkService = new NetworkService(settings);
        packetHistory.addToHistory(ByteBuffer.wrap("CAR_CAR_keep".getBytes(StandardCharsets.US_ASCII)));
        log.debug("Created server {}, static peers: {}", boundTo(), settings.getStaticPeers());
    }

    public void run() {
        while (!Thread.interrupted()) {
            try {
                runOnce();
            } catch (IOException e) {
                log.error("IO exception in loop", e);
                Utils.sleepMs(1000);
            }
        }
    }

    void runOnce() throws IOException {
        log.trace("");
        log.trace("Waiting for packets, {} peers", peers.size());
        networkService.select(this);
        cleanOutdatedPeers();
    }

    @Override
    public void handle(ByteBuffer buffer, SocketAddress from) throws IOException {
        log.trace("Received a packet from {}: {}", from, debugBufStr(buffer));
        if (acceptPacket(buffer, from)) {
            if (packetHistory.addToHistory(buffer)) {
                broadcastPacket(buffer, from);
            } else {
                log.trace("Not broadcasting duplicate packet");
            }
        }
    }

    private boolean acceptPacket(ByteBuffer buffer, SocketAddress from) {
        String magic = new String(buffer.array(), 0, Settings.MAGIC.length(), StandardCharsets.UTF_8);
        if (!checkMagic(magic)) {
            log.warn("New peer from {} not allowed: bad magic: {}", from, magic);
            return false;
        }
        Peer peer = peers.get(from);
        if (peer == null) {
            return acceptIncomingPeer(from);
        } else {
            peer.setLastUpdated(now());
            return true;
        }
    }

    private boolean checkMagic(String magicInPacket) {
        return Settings.MAGIC.equals(magicInPacket);
    }

    private boolean acceptIncomingPeer(SocketAddress from) {
        final int maxPeers = settings.getMaxPeers();
        if (peers.size() < maxPeers) {
            Peer peer = new Peer(from, now());
            peers.put(from, peer);
            log.info("Added new peer from {}", from);
            return true;
        } else {
            log.warn("New peer from {} not allowed: max peers limit of {} reached", from, maxPeers);
            return false;
        }
    }

    private void broadcastPacket(ByteBuffer buffer, SocketAddress from) throws IOException {
        for (SocketAddress peerAddr : peers.keySet()) {
            sendToPeer(from, peerAddr, buffer);
        }

        for (HostPort staticPeer : settings.getStaticPeers()) {
            sendToPeer(from, staticPeer.resolve(), buffer);
        }
    }

    private void sendToPeer(SocketAddress from, SocketAddress to, ByteBuffer buffer) throws IOException {
        if (!to.equals(from)) {
            log.trace("Sending last packet to {}", to);
            networkService.send(buffer, to);
            buffer.rewind();
        }
    }

    private void cleanOutdatedPeers() {
        Calendar oldEnough = getPeerOldThreshold();
        peers.entrySet().removeIf(e -> {
            boolean isOld = e.getValue().getLastUpdated().before(oldEnough.getTime());
            if (isOld) {
                log.info("Peer {} removed due to timeout", e.getKey());
            }
            return isOld;
        });
    }

    private Calendar getPeerOldThreshold() {
        Calendar oldEnough = Calendar.getInstance();
        oldEnough.add(Calendar.MILLISECOND, -settings.getPeerTimeout());
        return oldEnough;
    }

    InetSocketAddress boundTo() {
        return networkService.getBindAddress();
    }

    @Override
    public void close() throws IOException {
        networkService.close();
    }

    Settings getSettings() {
        return settings;
    }

    private static Date now() {
        return new Date();
    }
}
