package me.ippolitov.car.server;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.net.InetSocketAddress;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HostPort {
    private String host;
    private int port;
    
    public InetSocketAddress resolve() {
        return new InetSocketAddress(host, port);
    }
    
    public static HostPort parse(String hostPort) {
        String[] pair = hostPort.split(":");
        return new HostPort(pair[0], Integer.parseInt(pair[1]));
    }

    public static HostPort fromSocketAddress(InetSocketAddress socketAddress) {
        return new HostPort(socketAddress.getAddress().getHostAddress(), socketAddress.getPort());
    }

    @Override
    public String toString() {
        return host + ':' + port;
    }
}
