package me.ippolitov.car.server;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

public interface PacketHandler {
    void handle(ByteBuffer buffer, SocketAddress from) throws IOException;
}
