package me.ippolitov.car.server;

import lombok.Getter;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;

public class NetworkService implements Closeable {
    private final DatagramChannel channel;
    private final Selector selector;
    @Getter
    private InetSocketAddress bindAddress;
    private Settings settings;

    public NetworkService(Settings settings) throws IOException {
        this.settings = settings;
        this.channel = DatagramChannel.open(StandardProtocolFamily.INET);
        this.channel.bind(settings.getBindAddress());
        this.bindAddress = (InetSocketAddress) channel.getLocalAddress();
        this.selector = Selector.open();
        this.channel.configureBlocking(false);
        this.channel.register(selector, SelectionKey.OP_READ);

    }

    public void select(PacketHandler packetHandler) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        selector.select(settings.getSelectTimeout());
        final Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
        while (iterator.hasNext()) {
            SelectionKey key = iterator.next();
            if (key.isValid() && key.isReadable()) {
                assert key.channel() == channel;
                buffer.clear();
                SocketAddress from = channel.receive(buffer);
                buffer.flip();
                packetHandler.handle(buffer, from);
            }
            iterator.remove();
        }
    }

    public void send(ByteBuffer buffer, SocketAddress to) throws IOException {
        channel.send(buffer, to);
    }

    @Override
    public void close() throws IOException {
        channel.close();
        selector.close();
    }
}
