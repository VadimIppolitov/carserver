package me.ippolitov.car.server;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class Utils {
    static String debugBufStr(ByteBuffer buffer) {
        try {
            String content = simplify(getStringFromBuffer(buffer));
            return String.format("[p=%d, l=%d, c=%d, cont=%s]", buffer.position(), buffer.limit(), buffer.capacity(), content);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static String simplify(String string) {
        return string.replaceAll("[^A-Za-z0-9_]", "?");
    }

    static String getStringFromBuffer(ByteBuffer buffer) throws UnsupportedEncodingException {
        return new String(buffer.array(), buffer.position(), buffer.remaining(), "UTF-8");
    }

    public static void sleepMs(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
