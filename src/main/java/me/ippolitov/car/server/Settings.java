package me.ippolitov.car.server;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Settings {
    public static String MAGIC = "CAR";

    @Setter(value = AccessLevel.PACKAGE)
    private long selectTimeout = 60 * 1000;  //  Milliseconds

    @Setter(value = AccessLevel.PACKAGE)
    private int peerTimeout = 5 * 60 * 1000;  //  Milliseconds

    @Setter(value = AccessLevel.PACKAGE)
    private int maxPeers = 100;

    @Setter(value = AccessLevel.PACKAGE)
    private InetSocketAddress bindAddress = new InetSocketAddress(42465);

    private List<HostPort> staticPeers = new ArrayList<>();

    public void addStaticPeer(HostPort peer) {
        staticPeers.add(peer);
    }
}
