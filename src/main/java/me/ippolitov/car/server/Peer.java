package me.ippolitov.car.server;

import lombok.Getter;
import lombok.Setter;

import java.net.SocketAddress;
import java.util.Date;

@Getter
class Peer {
    private final SocketAddress addr;
    @Setter
    private Date lastUpdated;

    public Peer(SocketAddress addr, Date lastUpdated) {
        this.addr = addr;
        this.lastUpdated = lastUpdated;
    }
}
